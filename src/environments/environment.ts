import { Environment } from '@cisforce/utils/build';

export const environment = {
  type: Environment.Local,
  httpsPort: 4010,
  httpPort: 4010,
  databaseUrl: 'postgresql://postgres:root1234@127.0.0.1:5432/covid-help',
  apiUrl: 'http://localhost:4011/api',
  apiGraphql: 'http://localhost:4011/graphql',
  keyPath: '../../environment/cert/privkey.pem',
  certPath: '../../environment/cert/cert.pem',
  databaseOptions: {
    database: 'covid-help',
    dialect: 'postgres',
    username: 'postgres',
    password: 'root1234',
    define: {
      schema: 'public'
    }
  },
  email: {
    testAddreses: [
      'edolgov@cisforce.com',
      'edolgov@outlook.com',
      'edolgov.photo@gmail.com',
      'lazarev.business@gmail.com',
      'dlazarev@cisforce.com'
    ]
  },
  bot: {
    appId: 'bfab4bfc-eee1-446d-8060-28da0dcf4dd0',
    appSecret: 'rIoGNngB2u@R1IRa@8Hw=ALj@ETCmBh9',
    directLineSecret: 'JMVYRXDJbeY.rlPZ6I5ollkrsj6_uhP1B_ZVe4ZFa1CC4d59TFJTTuk'
  },
  geolocation: {
    apiUrl: null
  }
};
