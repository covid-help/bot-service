import { Global, HttpModule, Module } from '@nestjs/common';
import { UserConversationService } from './services';

@Global()
@Module({
  imports: [
    HttpModule
  ],
  providers: [
    UserConversationService
  ],
  exports: [
    UserConversationService
  ]
})
export class UserConversationModule {}
