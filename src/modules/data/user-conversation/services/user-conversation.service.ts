import { Actions } from '@cisforce/nest-kit/build';
import { uuid } from '@cisforce/utils/build';
import { HttpService, Injectable } from '@nestjs/common';
import { AxiosResponse } from 'axios';
import { environment } from '../../../../environments';
import { UserConversation } from '../../../../packages/data-models';
import { AbstractDataModelService, ConversationFlowInterface, UserConversationStatus } from '../../../common';

@Injectable()
export class UserConversationService extends AbstractDataModelService<UserConversation> {

  constructor(private actions: Actions, private httpService: HttpService) {
    super(UserConversation);
  }

  async createEntity(
    flow: ConversationFlowInterface,
    userId: string,
    sessionId: string,
    source: string,
    userIp: string
  ): Promise<UserConversation> {
    let userLocation: any;
    if (userIp && environment.geolocation.apiUrl) {
      try {
        const url: string = environment.geolocation.apiUrl + userIp;
        const response: AxiosResponse<any> = await this.httpService.get(url).toPromise();
        userLocation = response.data;
      } catch (ex) {}
    }
    const entity: UserConversation = new UserConversation({
      creationDateTime: new Date(),
      flow,
      id: uuid(),
      profile: {},
      sessionId,
      source,
      status: UserConversationStatus.Uncompleted,
      test: ['localhost', '127.0.0.1'].some(domain => source && source.includes(domain)),
      userId,
      userIp,
      userLocation
    });
    await this.saveEntity(entity);
    return entity;
  }

  async updateEntity(
    entity: UserConversation,
    full: boolean = true
  ): Promise<void> {
    if (full) {
      if ((entity.profile as any).dialogStack) {
        delete (entity.profile as any).dialogStack;
      }
      if ((entity.flow as any).dialogStack) {
        delete (entity.flow as any).dialogStack;
      }
      entity.profile = { ...entity.profile };
    }
    await this.saveEntity(entity);
  }
}
