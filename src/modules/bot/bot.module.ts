import { Global, Module } from '@nestjs/common';
import { BotController } from './controllers';
import { BotService, BotUtilsService } from './services';

@Global()
@Module({
  controllers: [
    BotController
  ],
  providers: [
    BotService,
    BotUtilsService
  ],
  exports: [
    BotService,
    BotUtilsService
  ]
})
export class BotModule {}
