export enum QuestionType {
  Text = 'Text',
  Choice = 'Choice'
}

export enum TextFieldType {
  String = 'String',
  Number = 'Number'
}
