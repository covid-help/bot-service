import { Question } from '../../common';
import { QuestionType, TextFieldType } from '../enums';

export interface QuestionInterface {
  question: Question;
  type: QuestionType;
  error?: string;
  previousMessages?: string[];
  nextMessages?: string[];
  data: any;
}

export interface ChoiceInterface {
  text: string;
  value: any;
  nextQuestion?: Question;
}

export interface QuestionChoiceInterface {
  text: string;
  choices: ChoiceInterface[];
}

export interface QuestionTextInterface {
  text: string;
  nextQuestion?: Question;
  type?: TextFieldType;
  options?: string[];
  validator?: (value: string) => boolean;
}
