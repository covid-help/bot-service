import { Question } from '../../../common';
import { QuestionType } from '../../enums';
import { QuestionInterface, QuestionTextInterface } from '../../interfaces';
import { EmailValidator } from '../../validators';

export const ContactQuestions: QuestionInterface[] = [
  {
    question: Question.Email,
    type: QuestionType.Text,
    error: 'Provided email address is not valid. Please enter valid email address.',
    data: {
      text: 'Please enter your email below so we could contact you with relevant information.',
      validator: EmailValidator,
    } as QuestionTextInterface
  }
];
