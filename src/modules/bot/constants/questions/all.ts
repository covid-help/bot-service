import { QuestionInterface } from '../../interfaces';
import { ContactQuestions } from './contact';
import { GeneralQuestions } from './general';

export const Questions: QuestionInterface[] = [
  ...GeneralQuestions,
  ...ContactQuestions
];
