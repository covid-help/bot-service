import { ProblemCategory, Purpose, Question } from '../../../common';
import { QuestionType, TextFieldType } from '../../enums';
import { QuestionChoiceInterface, QuestionInterface, QuestionTextInterface } from '../../interfaces';

export const GeneralQuestions: QuestionInterface[] = [
  {
    question: Question.Purpose,
    type: QuestionType.Choice,
    previousMessages: ['Welcome to Covid Help chat! We are doing our best to match people who are struggling now with those who can help. Stronger together!'],
    data: {
      text: 'Please choose what describes you best:',
      choices: [
        {
          text: 'I’m ok, and I can offer some help to those in need (every little is vital)',
          value: Purpose.OfferHelp,
          nextQuestion: Question.HelpText
        },
        {
          text: 'I need help, and I can also help people in other ways',
          value: Purpose.NeedAndOfferHelp,
          nextQuestion: Question.ProblemText
        },
        {
          text: 'I need help, but I cannot offer any help to other people',
          value: Purpose.NeedHelp,
          nextQuestion: Question.ProblemText
        },
      ],
    } as QuestionChoiceInterface
  },
  {
    question: Question.ProblemText,
    type: QuestionType.Text,
    data: {
      text: 'What help do you need? Please describe in detail.',
      nextQuestion: Question.Email
    } as QuestionTextInterface,
  },
  {
    question: Question.HelpText,
    type: QuestionType.Text,
    data: {
      text: 'How could you help to those who are struggling? (example: give food, provide some service for free, buy something from local small business etc.) Please describe in detail.'
    } as QuestionTextInterface,
  }
];
