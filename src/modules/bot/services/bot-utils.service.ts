import { Injectable } from '@nestjs/common';
import { TurnContext } from 'botbuilder';
import { UserConversation } from '../../../packages/data-models';
import { ClientDataInterface } from '../../common';
import { QuestionInterface } from '../interfaces';

@Injectable()
export class BotUtilsService {

  getClientData(context: TurnContext): ClientDataInterface {
    let data: ClientDataInterface = {
      session: { id: null, source: null, ip: null },
      user: { id: null, name: null },
    };
    if (context.activity.from.id) {
      data.user.id = context.activity.from.id;
    }
    if (context.activity.from.name) {
      try {
        data = JSON.parse(context.activity.from.name);
      } catch (ex) {}
    }
    return data;
  }
}
