import { Actions } from '@cisforce/nest-kit/build';
import { EmailDto, Environment } from '@cisforce/utils/build';
import { Injectable } from '@nestjs/common';
import { ActivityHandler, ConversationState, MemoryStorage, StatePropertyAccessor, TurnContext, UserState } from 'botbuilder';
import { environment } from '../../../environments';
import { UserConversation } from '../../../packages/data-models';
import { ClientDataInterface, ConversationFlowInterface, Purpose, Question, UserConversationStatus, UtilsService } from '../../common';
import { UserConversationService } from '../../data';
import { Questions } from '../constants';
import { ChoicePromptDialog } from '../dialogs';
import { QuestionType, TextFieldType } from '../enums';
import { ChoiceInterface, QuestionChoiceInterface, QuestionInterface, QuestionTextInterface } from '../interfaces';
import { BotUtilsService } from './bot-utils.service';

// The accessor names for the conversation flow and user profile state property accessors.
const CONVERSATION_FLOW_PROPERTY = 'CONVERSATION_FLOW_PROPERTY';

const DEFAULT_FLOW: ConversationFlowInterface = {
  currentQuestion: null,
  userConversationId: null,
};

@Injectable()
export class BotService extends ActivityHandler {

  conversationFlow: StatePropertyAccessor<ConversationFlowInterface>;
  conversationState: ConversationState;
  memoryStorage: MemoryStorage;
  userState: UserState;

  constructor(
    private botUtilsService: BotUtilsService,
    private userConversationService: UserConversationService,
    private utilsService: UtilsService
  ) {
    super();

    this.memoryStorage = new MemoryStorage();
    this.conversationState = new ConversationState(this.memoryStorage);
    this.userState = new UserState(this.memoryStorage);

    // The state property accessors for conversation flow and user profile.
    this.conversationFlow = this.conversationState.createProperty(
      CONVERSATION_FLOW_PROPERTY
    );

    this.onMessage(async (context: TurnContext, next: any) => {
      const flow: ConversationFlowInterface = await this.conversationFlow.get(
        context,
        DEFAULT_FLOW
      );
      await this.processMessage(flow, context);
      await next();
    });
  }

  /**
   * Process message
   */

  async processMessage(
    flow: ConversationFlowInterface,
    context: TurnContext
  ): Promise<void> {
    let input: string = context.activity.text.trim();
    const data: ClientDataInterface = this.botUtilsService.getClientData(context);
    const userId: string = data.user.id;
    const sessionId: string = data.session.id;
    const sessionSource: string = data.session.source || null;
    const userIp: string = data.session.ip || null;
    let conversation: UserConversation;
    if (flow.userConversationId) {
      conversation = await this.userConversationService.getEntityById(
        flow.userConversationId
      );
    }
    if (!conversation) {
      if (userId) {
        const conversations: UserConversation[] = await this.userConversationService.getAllEntities(
          {
            where: sessionId ? { userId, sessionId } : { userId },
            order: [['creationDateTime', 'DESC']],
          }
        );
        if (conversations.length > 0) {
          // Restore state from DB
          conversation = conversations[0];
          Object.assign(flow, conversation.flow);
        }
      }
      if (!conversation) {
        conversation = await this.userConversationService.createEntity(
          flow,
          userId,
          sessionId,
          sessionSource,
          userIp
        );
        flow.userConversationId = conversation.id;
      }
    }

    if (conversation.status !== UserConversationStatus.Uncompleted) {
      return;
    }

    // Process answer and update profile
    flow.currentQuestion = flow.currentQuestion || Questions[0].question;
    const questionData: QuestionInterface = Questions.find(
      (item) => item.question === flow.currentQuestion
    );
    let nextQuestion: Question;
    let errorMessages: string[] = [];

    let currentFieldName: string = questionData.question;

    if (questionData.type === QuestionType.Choice) {
      const selectedChoice: ChoiceInterface = (questionData.data as QuestionChoiceInterface).choices.find(
        (item: ChoiceInterface) => item.text.toLowerCase() === input.toLowerCase()
      );

      if (!selectedChoice) {
        await this.sendError(context, questionData.error);
        return;
      }

      conversation.profile[currentFieldName] = selectedChoice.value;

      if (selectedChoice.nextQuestion) {
        nextQuestion = selectedChoice.nextQuestion;
      }
    } else if (questionData.type === QuestionType.Text) {
      const textData: QuestionTextInterface = questionData.data as QuestionTextInterface;
      if (textData.options && textData.options) {
        const option = textData.options.find(
          (item: string) => item.toLowerCase() === input.toLowerCase()
        );
        if (option) {
          input = option;
        } else {
          await this.sendError(context, questionData.error);
          return;
        }
      }
      if (textData.validator) {
        if (!textData.validator(input)) {
          await this.sendError(context, questionData.error);
          return;
        }
      }

      conversation.profile[currentFieldName] =
        textData.type && textData.type === TextFieldType.Number
          ? Number(input)
          : input;

      if (textData.nextQuestion) {
        nextQuestion = textData.nextQuestion;
      }
    }

    // Send error messages
    if (errorMessages) {
      for (const message of errorMessages) {
        await context.sendActivity(message);
      }
    }

    // Send next messages
    if (questionData.nextMessages) {
      for (const message of questionData.nextMessages) {
        await context.sendActivity(message);
      }
    }

    // CUSTOM LOGIC for specific questions
    if (flow.currentQuestion === Question.Purpose) {
      if (conversation.profile.purpose !== Purpose.NeedHelp) {
        await context.sendActivity('Thank you, we really appreciate it!');
      }
    } else if (flow.currentQuestion === Question.ProblemText) {
      if (conversation.profile.purpose === Purpose.NeedAndOfferHelp) {
        nextQuestion = Question.HelpText;
      }
    } else if (flow.currentQuestion === Question.Email) {
      conversation.email = conversation.profile.email;
      conversation.test =
        conversation.test ||
        environment.email.testAddreses.includes(conversation.profile.email);
    }

    // Ask next question if it exists and update conversation object in DB
    const res: boolean = await this.askNextQuestion(
      flow,
      context,
      nextQuestion
    );
    if (!res) {
      conversation.status = UserConversationStatus.Completed;
      conversation.completeDateTime = new Date();
      conversation.updateDateTime = new Date(conversation.completeDateTime);
      await this.sendCompleted(context);
    } else {
      conversation.updateDateTime = new Date();
    }
    conversation.flow = flow;
    if (!conversation.userId) {
      conversation.userId = userId;
    }
    await this.userConversationService.updateEntity(conversation);
  }

  /**
   * Context utils
   */

  async run(context: TurnContext): Promise<void> {
    await super.run(context);

    // Save any state changes. The load happened during the execution of the Dialog.
    await this.conversationState.saveChanges(context, false);
    await this.userState.saveChanges(context, false);
  }

  async deleteContext(context: TurnContext): Promise<void> {
    await this.conversationState.delete(context);
  }

  async onEventActivity(context: TurnContext): Promise<void> {
    super.onEventActivity(context);
    if (context.activity.name === 'webchat/join') {
      const flow: ConversationFlowInterface = await this.conversationFlow.get(
        context,
        DEFAULT_FLOW
      );
      if (!flow.currentQuestion) {
        await this.askQuestion(Questions[0].question, flow, context);
      }
    }
  }

  /**
   * Question utils
   */

  private async askQuestion(
    question: Question,
    flow: ConversationFlowInterface,
    context: TurnContext
  ): Promise<boolean> {
    const questionData: QuestionInterface = Questions.find(
      (item) => item.question === question
    );

    if (questionData) {
      if (questionData.previousMessages) {
        for (const previousMessage of questionData.previousMessages) {
          await context.sendActivity(previousMessage);
        }
      }
      const message: string = questionData.data.text;
      if (questionData.type === QuestionType.Choice) {
        const dialog: ChoicePromptDialog = new ChoicePromptDialog(
          message,
          questionData.data.choices
        );
        await dialog.run(context, this.conversationFlow);
      } else if (questionData.type === QuestionType.Text) {
        await context.sendActivity(message);
      }
      flow.currentQuestion = question;
      return true;
    } else {
      return false;
    }
  }

  private async askNextQuestion(
    flow: ConversationFlowInterface,
    context: TurnContext,
    nextQuestion?: Question
  ): Promise<boolean> {
    if (nextQuestion) {
      return this.askQuestion(
        nextQuestion,
        flow,
        context
      );
    }
    const currentIndex: number = Questions.findIndex(
      (item) => item.question === flow.currentQuestion
    );
    if (currentIndex < Questions.length - 1) {
      return this.askQuestion(
        Questions[currentIndex + 1].question,
        flow,
        context
      );
    }
    return false;
  }

  private async sendError( context: TurnContext, error?: string): Promise<void> {
    await context.sendActivity(error || 'Your answer is not correct, please try again.');
  }

  private async sendCompleted(context: TurnContext): Promise<void> {
    await context.sendActivity('Thank you! We will be in touch with you shortly.');
  }
}
