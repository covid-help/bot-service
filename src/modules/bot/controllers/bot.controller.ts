import { AbstractController } from '@cisforce/nest-kit/build';
import { Controller, HttpCode, HttpStatus, Post, Req, Res } from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { BotFrameworkAdapter, TurnContext } from 'botbuilder';
import { Request, Response } from 'express';
import { environment } from '../../../environments';
import { UserConversation } from '../../../packages/data-models';
import { ConversationFlowInterface } from '../../common';
import { UserConversationService } from '../../data';
import { BotService } from '../services';

@Controller()
@ApiTags('bot')
export class BotController extends AbstractController {
  // Create adapter.
  // See https://aka.ms/about-bot-adapter to learn more about adapters.
  adapter: any;

  constructor(
    private botService: BotService,
    private userConversationService: UserConversationService
  ) {
    super();

    this.adapter = new BotFrameworkAdapter({
      appId: environment.bot.appId,
      appPassword: environment.bot.appSecret
    });

    // Catch-all for errors.
    this.adapter.onTurnError = async (context: TurnContext, error: any) => {
      // This check writes out errors to console log .vs. app insights.
      // NOTE: In production environment, you should consider logging this to Azure
      //       application insights.
      console.error(`\n [onTurnError] unhandled error: ${error}`);

      // Send a trace activity, which will be displayed in Bot Framework Emulator
      await context.sendTraceActivity(
        'OnTurnError Trace',
        `${error}`,
        'https://www.botframework.com/schemas/error',
        'TurnError'
      );

      let hasConversation: boolean = false;

      try {
        const count: number = await this.userConversationService.countEntities({
          where: { userId: context.activity.from.id }
        });
        if (count > 0) {
          hasConversation = true;
        }
      } catch (ex) {}

      let flow: ConversationFlowInterface = null;

      try {
        flow = await this.botService.conversationFlow.get(context);

        if (!hasConversation && flow && flow.currentQuestion) {
          hasConversation = true;
        }
      } catch (ex) {}

      let errorMessage: string = 'An error has happened while delivering or processing the message. ';
      errorMessage += hasConversation
        ? 'Please try to answer again or refresh the page and restart this assessment.'
        : 'Please refresh the page and start this assessment again.';

      // Send a message to the user
      await context.sendActivity(errorMessage);
    };
  }

  @Post('messages')
  @HttpCode(HttpStatus.CREATED)
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'Create message'
  })
  async createMessage(
    @Req() req: Request,
    @Res() res: Response
  ): Promise<void> {
    this.adapter.processActivity(req, res, async (context: any) => {
      // Route to main dialog.
      await this.botService.run(context);
    });
  }
}
