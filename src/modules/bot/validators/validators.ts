import { Validator } from 'class-validator';

export const EmailValidator = function(value: string): boolean {
  const validator: Validator = new Validator();
  value = value ? value.trim() : value;
  return validator.isEmail(value);
}
