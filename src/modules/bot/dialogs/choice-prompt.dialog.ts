import { StatePropertyAccessor, TurnContext } from 'botbuilder';
import {
  ChoiceFactory,
  ChoicePrompt,
  ComponentDialog,
  DialogContext,
  DialogSet,
  DialogTurnResult,
  DialogTurnStatus,
  ListStyle,
  WaterfallDialog,
  WaterfallStepContext
} from 'botbuilder-dialogs';
import { ChoiceInterface, QuestionChoiceInterface } from '../interfaces';

const CHOICE_PROMPT = 'CHOICE_PROMPT';
const WATERFALL_DIALOG = 'WATERFALL_DIALOG';

export class ChoicePromptDialog extends ComponentDialog {
  constructor(private message: string, private choices: ChoiceInterface[]) {
    super('ChoicePromptDialog');
    this.addDialog(new ChoicePrompt(CHOICE_PROMPT));

    this.addDialog(
      new WaterfallDialog(WATERFALL_DIALOG, [this.step.bind(this)])
    );

    this.initialDialogId = WATERFALL_DIALOG;
  }

  /**
   * The run method handles the incoming activity (in the form of a TurnContext) and passes it through the dialog system.
   * If no dialog is active, it will start the default dialog.
   * @param {*} turnContext
   * @param {*} accessor
   */
  public async run(context: TurnContext, accessor: StatePropertyAccessor) {
    const dialogSet = new DialogSet(accessor);
    dialogSet.add(this);

    const dialogContext: DialogContext = await dialogSet.createContext(context);
    const results: DialogTurnResult = await dialogContext.continueDialog();
    if (results.status === DialogTurnStatus.empty) {
      await dialogContext.beginDialog(this.id);
    }
  }

  private async step(stepContext: WaterfallStepContext) {
    // WaterfallStep always finishes with the end of the Waterfall or with another dialog; here it is a Prompt Dialog.
    // Running a prompt here means the next WaterfallStep will be run when the users response is received.
    await stepContext.prompt(CHOICE_PROMPT, {
      choices: ChoiceFactory.toChoices(this.choices.map(item => item.text)),
      prompt: this.message,
      style: ListStyle.heroCard
    });
    return await stepContext.endDialog();
  }
}
