import { Global, Module } from '@nestjs/common';
import { UtilsController } from './controllers';
import { AutomapperService, PermissionsService, UtilsService } from './services';

@Global()
@Module({
  controllers: [
    UtilsController
  ],
  providers: [
    AutomapperService,
    PermissionsService,
    UtilsService
  ],
  exports: [
    AutomapperService,
    PermissionsService,
    UtilsService
  ]
})
export class CommonModule {}
