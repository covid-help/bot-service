import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { PermissionsService } from '../services';

@Injectable()
export class PermissionsGuard implements CanActivate {

  private permissionsService: PermissionsService = new PermissionsService();

  constructor(private reflector: Reflector) {
  }

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const permissions: any[] = this.reflector.get<any[]>('permissions', context.getHandler());
    const request: any = context.switchToHttp().getRequest();
    return this.permissionsService.checkUserPermissions(request.user.context.user, permissions);
  }
}
