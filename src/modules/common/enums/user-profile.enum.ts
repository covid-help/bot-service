export enum Purpose {
  NeedHelp = 'NeedHelp',
  OfferHelp = 'OfferHelp',
  NeedAndOfferHelp = 'NeedAndOfferHelp'
}

export enum ProblemCategory {
  Category1 = 'Category1',
  Category2 = 'Category2'
}

export enum YesNo {
  Yes = 'Yes',
  No = 'No'
}
