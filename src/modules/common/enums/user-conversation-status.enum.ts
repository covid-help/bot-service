export enum UserConversationStatus {
  Uncompleted = 'Uncompleted',
  Completed = 'Completed'
}
