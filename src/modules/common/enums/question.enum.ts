export enum Question {
  Purpose = 'purpose',
  HelpText = 'helpText',
  ProblemText = 'problemText',
  Email = 'email'
}
