import { AbstractDataModelService as NestKitAbstractDataModelService, DataModelServiceParamsInterface } from '@cisforce/nest-kit/build';
import { Model } from 'sequelize-typescript';
import { ModelType } from 'sequelize-typescript/dist/model/model/model-type';
import { environment } from '../../../environments';

export abstract class AbstractDataModelService<T extends Model<T>> extends NestKitAbstractDataModelService<T> {
  constructor(modelType: ModelType<T>) {
    super({
      apiGraphql: environment.apiGraphql,
      databaseSchema: environment.databaseOptions.define.schema,
      databaseUrl: environment.databaseUrl,
      modelType
    } as DataModelServiceParamsInterface<T>);
  }
}
