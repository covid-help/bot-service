import { AbstractApolloService as NestAbstractApolloService, ApolloServiceParamsInterface } from '@cisforce/nest-kit/build';
import { environment } from '../../../environments';

export abstract class AbstractApolloService extends NestAbstractApolloService {
  constructor() {
    super({
      apiGraphql: environment.apiGraphql,
      databaseSchema: environment.databaseOptions.define.schema,
      databaseUrl: environment.databaseUrl
    } as ApolloServiceParamsInterface);
  }
}
