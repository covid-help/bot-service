import { createGenericMapping, mapDtoToEntity as automapperMapDtoToEntity, MappingDirection } from '@cisforce/utils/build';
import { Injectable } from '@nestjs/common';

const automapper = require('automapper-ts');

@Injectable()
export class AutomapperService {

  constructor() {
    this.createMappings();
  }

  // tslint:disable-next-line
  mapDtoToEntity<D, E>(dtoType: { new (): D; }, entityType: { new (): E; }, object: D, simpleMapping?: boolean): E {
    const res: E = automapperMapDtoToEntity(dtoType, entityType, object, automapper);
    if (simpleMapping) {
      const ignoreFields: string[] = automapper._mappings[dtoType.name + entityType.name].ignoreFields || [];
      for (const key of Object.keys(object)) {
        if (ignoreFields.indexOf(key) === -1) {
          res[key] = object[key];
        }
      }
    }
    return res;
  }

  private createMappings(): void {
  }
}
