import { AbstractController } from '@cisforce/nest-kit/build';
import { Controller, Get, HttpCode, HttpStatus } from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { IpAddress } from '../decorators';

@Controller('utils')
@ApiTags('utils')
export class UtilsController extends AbstractController {

  @Get('ip-address')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Get user ip address'
  })
  async getIpAddress(@IpAddress() ip: string): Promise<{ ip: string }> {
    return { ip };
  }
}
