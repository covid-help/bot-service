import { ConversationFlowInterface } from './conversation-flow.interface';
import { UserProfileInterface } from './user-profile.interface';

export interface ConversationInterface {
  flow: ConversationFlowInterface;
  profile: UserProfileInterface;
}
