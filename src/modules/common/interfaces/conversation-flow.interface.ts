import { Question } from '../enums';

export interface ConversationFlowInterface {
  currentQuestion: Question;
  userConversationId: string;
}
