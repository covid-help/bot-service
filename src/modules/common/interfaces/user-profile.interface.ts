import { ProblemCategory, Purpose, YesNo } from '../enums';

export interface UserProfileInterface {
  purpose?: Purpose;
  problemCategory?: ProblemCategory;
  problemText?: string;
  email?: string;
}
