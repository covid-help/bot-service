export interface ClientDataInterface {
  session: {
    id: string;
    source: string;
    ip: string;
  };
  user: {
    id: string;
    name?: string;
  };
}
