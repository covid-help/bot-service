import { ActionsModule, AuthModule, StatusModule } from '@cisforce/nest-kit/build';
import { HttpModule, Module } from '@nestjs/common';
import { SECRET_KEY } from './constants';
import * as Modules from './modules';

export const modules = [
  // Modules
  Modules.BotModule,
  Modules.CommonModule,
  // Data modules
  Modules.UserConversationModule
];

@Module({
  imports: [
    // nest kit imports
    ActionsModule,
    AuthModule.forRoot(SECRET_KEY),
    StatusModule,
    // nest imports
    HttpModule,
    // modules
    ...modules
  ]
})
export class ApplicationModule {}
