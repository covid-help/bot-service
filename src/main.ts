import { bootstrapNestApp } from '@cisforce/nest-kit/build';
import { Environment } from '@cisforce/utils/build';
import * as Bluebird from 'bluebird';
import 'cross-fetch/polyfill';
import * as fs from 'fs';
import * as path from 'path';
import { ApplicationModule } from './app.module';
import { environment } from './environments';
import { connectDatabaseDefault } from './packages/database-management/utils';

global.Promise = Bluebird;

async function bootstrap() {
  connectDatabaseDefault();
  await bootstrapNestApp({
    databaseUrl: environment.databaseUrl,
    http: { port: environment.httpPort },
    https:
      environment.type === Environment.Local
        ? null
        : {
            cert: fs.readFileSync(
              path.resolve(__dirname, environment.certPath)
            ),
            key: fs.readFileSync(path.resolve(__dirname, environment.keyPath)),
            port: environment.httpsPort
          },
    module: ApplicationModule,
    rateLimit: { windowMs: 60 * 1000, max: 5000 },
    swagger: {
      title: 'Covid Help api',
      version: '1.0'
    }
  });
}
bootstrap();
