import { Column, DataType, IsUUID, Model, PrimaryKey, Table } from 'sequelize-typescript';
import { ConversationFlowInterface, UserConversationStatus, UserProfileInterface } from '../../../modules/common';

@Table({ timestamps: false, freezeTableName: true })
export class UserConversation extends Model<UserConversation> {

  @IsUUID(4)
  @PrimaryKey
  @Column({ type: DataType.UUID })
  id: string;

  @Column
  email: string;

  @Column
  creationDateTime: Date;

  @Column
  updateDateTime: Date;

  @Column
  completeDateTime: Date;

  @Column
  status: UserConversationStatus;

  @Column({ type: DataType.JSON })
  flow: ConversationFlowInterface;

  @Column({ type: DataType.JSON })
  profile: UserProfileInterface;

  @Column
  userId: string;

  @Column
  sessionId: string;

  @Column
  userIp: string;

  @Column({ type: DataType.JSON })
  userLocation: any;

  @Column
  source: string;

  @Column
  test: boolean;
}
