/* tslint:disable */

const exec = require('child_process').exec;
const fs = require('fs');

if (fs.existsSync('versions/latest/migration.ts')) {
  exec('node ../../../../node_modules/ts-node/dist/bin.js versions/latest/migration.ts', { maxBuffer: 1024 * 500 * 10 }, (error, stdout, stderr) => {
    if (stdout) {
      console.log(stdout);
    }
    if (stderr) {
      console.log(stderr);
    }
    if (error) {
      console.log('Migration script error: ' + error);
    }
});
} else {
  console.log('There is no any migration scripts to execute');
}
