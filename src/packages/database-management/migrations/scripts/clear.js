/* tslint:disable */

const fs = require('fs');

const files = [
  'migrate-scheme.sql',
  'local-scheme.sql',
  'test-scheme.sql',
  'prod-scheme.sql',
  'local-data.sql',
  'test-data.sql',
  'prod-data.sql',
  'local-all.sql',
  'test-all.sql',
  'prod-all.sql',
  'migration.ts'
];

for (const file of files) {
  if (fs.existsSync(file)) {
    fs.unlinkSync(file);
  }
}
return;
