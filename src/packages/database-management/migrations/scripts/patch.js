/* tslint:disable */

const versiony = require('../../../../../node_modules/versiony/lib/index');
const fs = require('fs');
const rimraf = require('rimraf');

const migrateSchemeSqlName = 'migrate-scheme.sql';
const sqlFiles = [
  migrateSchemeSqlName,
  'local-scheme.sql',
  'test-scheme.sql',
  'prod-scheme.sql',
  'local-data.sql',
  'test-data.sql',
  'prod-data.sql',
  'local-all.sql',
  'test-all.sql',
  'prod-all.sql'
];

fs.readFile(migrateSchemeSqlName, 'utf8', function(err, data) {
  if (err) throw err;
  if (!data) {
    for (const file of sqlFiles) {
      if (fs.existsSync(file)) {
        fs.unlinkSync(file);
      }
    }
    return;
  }
  rimraf('../versions/latest', function () {
    versiony.patch().from('../package.json').to().end();
    const version = versiony.from('../package.json').getVersion();
    const versionDir = `../versions/migration-${new Date().toJSON().slice(0,10).replace(/-/g,'.')}-v${version.major}.${version.minor}.${version.patch}`;
    if (!fs.existsSync(versionDir)){
      fs.mkdirSync(versionDir);
    }
    const latesDir = '../versions/latest';
    if (!fs.existsSync(latesDir)){
      fs.mkdirSync(latesDir);
    }

    for (const file of sqlFiles) {
      if (fs.existsSync(file)) {
        fs.createReadStream(file).pipe(fs.createWriteStream(versionDir + '/' + file));
        fs.createReadStream(file).pipe(fs.createWriteStream(latesDir + '/' + file));
      }
    }
    for (const file of sqlFiles) {
      if (fs.existsSync(file)) {
        fs.unlinkSync(file);
      }
    }
  });
});
