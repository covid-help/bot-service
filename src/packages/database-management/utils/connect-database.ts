import importToArray from 'import-to-array';
import { Sequelize, SequelizeOptions } from 'sequelize-typescript';
import { environment } from '../../../environments';
import { DataModels } from '../../data-models';

const DEFAULT_OPTIONS: SequelizeOptions = environment.databaseOptions as SequelizeOptions;

export function addModels(ormInstance: Sequelize): Sequelize {
  ormInstance.addModels(importToArray(DataModels));
  return ormInstance;
}

export function connectDatabaseDefault(): Sequelize {
  const ormInstance: Sequelize = new Sequelize(DEFAULT_OPTIONS);
  return this.addModels(ormInstance);
}

export function connectDatabaseWithPassword(password: string): Sequelize {
  const ormInstance: Sequelize = new Sequelize({ ...DEFAULT_OPTIONS, password });
  return this.addModels(ormInstance);
}

export function connectDatabase(options: SequelizeOptions): Sequelize {
  const ormInstance: Sequelize = new Sequelize(options);
  return this.addModels(ormInstance);
}
