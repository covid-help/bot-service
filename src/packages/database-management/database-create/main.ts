import * as Bluebird from 'bluebird';
import { Sequelize } from 'sequelize-typescript';
import { environment } from '../../../environments';
import { connectDatabaseDefault } from '../utils';

global.Promise = Bluebird;

const ormInstance: Sequelize = connectDatabaseDefault();

Promise.resolve()
  .then(() => {
    return ormInstance.dropSchema(environment.databaseOptions.define.schema, {});
  })
  .then(() => {
    return ormInstance.createSchema(environment.databaseOptions.define.schema, {});
  })
  .then(() => {
    return ormInstance.sync({ force: true });
  }).then(async () => {
    process.exit();
  });
